Majority of the young researchers in developing countries are talented; however, many of them are struggling to match their skills to an application area where they can be relevant in terms of research. As a result, these young talents are underdeveloped and are unable to tackle the problems around them. To bridge this gap, the idea is to connect them with international academics for remote mentoring/supervision, so that they can learn, explore and develop their passion for research. 

In this project, you will develop a web platform for handling the remote mentoring/supervision. Very broadly, the website should enable academics propose projects, and enable the young researchers create profiles highlighting their skills and interests. Other potential directions include developing and implementing an algorithm that automates the matching process based on certain criteria, integrating a cloud-based Jupyter notebook for collaborative coding, an online whiteboard, and so on.

This project extends the goals of the PWSAfrica initiative -- an international outreach supported by the School of Computing Science, University of Glasgow. For more information, see https://www.pwsafrica.org/ and https://www.gla.ac.uk/schools/computing/international/internationaloutreach/.


